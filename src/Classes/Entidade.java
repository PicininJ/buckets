/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import javahash.JAvaHash;

/**
 *
 * @author Aluno
 */
public class Entidade
{

    private int info;

    public Entidade(int info)
    {
        this.info = info;
    }

    public int getInfo()
    {
        return info;
    }

    public void setInfo(int info)
    {
        this.info = info;
    }

    @Override
    public int hashCode()
    {
        return info / JAvaHash.Nmax;
    }

    @Override
    public String toString()
    {
        return Integer.toString(info) + "(" + hashCode() + ")";
    }
}
